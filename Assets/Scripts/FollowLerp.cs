﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowLerp : MonoBehaviour {


public GameObject objetivo;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp(transform.position,objetivo.transform.position,0.07f);
		transform.rotation = objetivo.transform.rotation;
	}
}
