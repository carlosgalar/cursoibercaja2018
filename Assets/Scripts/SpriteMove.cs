﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMove : MonoBehaviour
{

    public Animator animador;

    // Use this for initialization
    void Start()
    {
        animador = (Animator)GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)){
            animador.SetBool("Escupir", true);
			animador.SetBool("Andar", false);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow)){
            animador.SetBool("Andar", true);
			 animador.SetBool("Escupir", false);
        }
        

    }
}
