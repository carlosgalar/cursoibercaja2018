﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorScript : MonoBehaviour {

	public Animator animator;
	public string parameterName="Vertical Axis";
	public string axisName="Vertical";
	public float axisValue;
	public Rigidbody rg;


	// Update is called once per frame
	void Update () {
		axisValue = Input.GetAxis(axisName);
		animator.SetFloat (parameterName,axisValue);
		
	Vector3 avance = Input.GetAxis ("Vertical") * transform.forward * 5f*(axisValue>0?axisValue:-axisValue);
		rg.velocity = avance;
		rg.AddExplosionForce(100,avance,10);
	Vector3 rotacion = Input.GetAxis ( "Horizontal" ) * Vector3.up * 100f * Time.deltaTime;
		transform.Rotate ( rotacion );

			if(Input.GetKeyDown(KeyCode.Space)){
				animator.SetBool ("SpaceKey",true);
			}else{
				animator.SetBool ("SpaceKey",false);
			}
			if(Input.GetKey(KeyCode.Q)){
				animator.SetTrigger ("kick");
		    }


	}

}
