﻿// Script: ControladorTransform4.cs
// Descripción: Controlador basico de transform teniendo en cuenta el deltaTime y utilizando las variables
//  estaticas de la clase Vector3 para indicar las direcciones. Las teclas se obtienen por eje y se utilizan
//  variables auxiliares Vector3 para guardar el resultado
// Autor: Ivan Garcia Subero
// Fecha: 25.03.14
// Licencia: Dominio público
// Dependencias: Ninguna
// Por hacer: Nada

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControladorTransforms4 : MonoBehaviour {

	public float giroX = 1550f;
	public float giroY = 1500f;
	public float velocidadZ = 1000f;
	public float velocidadCamara = 1000f;
	public float acelerar = 1;
	public float numBolas=0;

public Camera camara;
	
	public GameObject bomba; 
	public GameObject helice; 
	public GameObject hijo; 
	public GameObject fantasma; 
	public GameObject posBombas; 

	public Text velocidadT;
public Rigidbody rg;

	void Start(){
		 Cursor.visible = false;
	}


	// Update is called once per frame
	void Update ( ) {

		Debug.Log(Time.deltaTime);

		//Vector3 avance = Input.GetAxis ("Vertical") * hijo.transform.forward * velocidadZ *acelerar* Time.deltaTime;
		//transform.Translate ( avance,Space.World );

		Vector3 avance = Input.GetAxis ("Vertical") * Vector3.forward * velocidadZ *acelerar* Time.deltaTime;
		transform.Translate ( avance );

		Vector3 rotacion = Input.GetAxis ( "Mouse X" ) * Vector3.up * giroX * Time.deltaTime;
		transform.Rotate ( rotacion );

		Vector3 rotacion2 = Input.GetAxis ( "Mouse Y" ) * Vector3.left * giroY * Time.deltaTime;
		transform.Rotate ( rotacion2);

		Vector3 rotacionpivote = Input.GetAxis ( "Loop" ) * Vector3.forward * velocidadZ * Time.deltaTime;
		hijo.transform.Rotate ( rotacionpivote);
		
		Vector3 rotacion3 = Input.GetAxis ( "Loop" ) * Vector3.forward * velocidadZ * Time.deltaTime;
		transform.Rotate ( -rotacion3);
		
		Vector3 rotacionCamara = Input.GetAxis ( "Mouse ScrollWheel" ) * Vector3.forward * velocidadCamara * Time.deltaTime;
		camara.transform.Translate ( rotacionCamara);


		if ( Input.GetKeyDown ( KeyCode.Space ) ) {
			 Instantiate(bomba, posBombas.transform.position, transform.rotation);
	 numBolas++;
		}

	hijo.transform.rotation=	Quaternion.Lerp(hijo.transform.rotation,fantasma.transform.rotation,0.05f);
	//transform.rotation=	Quaternion.Lerp(transform.rotation,fantasma.transform.rotation,0.05f);

	 
	velocidadT.text="Bolas lanzadas: "+numBolas;

		if(Input.anyKey){
		helice.transform.Rotate ( Vector3.forward*1000f);
		}
		helice.transform.Rotate ( Vector3.forward*20f);

	}

 CursorLockMode wantedMode;

    // Apply requested cursor state
    void SetCursorState()
    {
        Cursor.lockState = wantedMode;
        // Hide cursor when locking
        Cursor.visible = (CursorLockMode.Locked != wantedMode);
    }

    void OnGUI()
    {
        GUILayout.BeginVertical();
        // Release cursor on escape keypress
        if (Input.GetKeyDown(KeyCode.Escape))
            Cursor.lockState = wantedMode = CursorLockMode.None;

        switch (Cursor.lockState)
        {
            case CursorLockMode.None:
                GUILayout.Label("Cursor is normal");
                if (GUILayout.Button("Lock cursor"))
                    wantedMode = CursorLockMode.Locked;
                if (GUILayout.Button("Confine cursor"))
                    wantedMode = CursorLockMode.Confined;
                break;
            case CursorLockMode.Confined:
                GUILayout.Label("Cursor is confined");
                if (GUILayout.Button("Lock cursor"))
                    wantedMode = CursorLockMode.Locked;
                if (GUILayout.Button("Release cursor"))
                    wantedMode = CursorLockMode.None;
                break;
            case CursorLockMode.Locked:
                GUILayout.Label("Cursor is locked");
                if (GUILayout.Button("Unlock cursor"))
                    wantedMode = CursorLockMode.None;
                if (GUILayout.Button("Confine cursor"))
                    wantedMode = CursorLockMode.Confined;
                break;
        }

        GUILayout.EndVertical();

        SetCursorState();
    }

}

