﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptDestroy : MonoBehaviour {

	public GameObject explosion;

	// Use this for initialization
	void Start () {
		
	}
	
	void OnCollisionEnter (Collision col)
    {
        Debug.Log(col.gameObject.name);
         if(col.gameObject.name == "Megaton_Atomic_Bomb(Clone)" || col.gameObject.name == "Caja" || col.gameObject.name == "Terrain"){
            Instantiate(explosion,this.transform.position,new Quaternion(0,0,0,0));
			Destroy(gameObject,3);
        }
    }
}
