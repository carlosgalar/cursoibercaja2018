﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptPistola : MonoBehaviour
{



    public int municionMaxima = 8;
    public List<int> cargadores= new List<int>();
    public int indiceCargadorPistola = 0;
    public int indiceCargadorSelect = 0;
    public float enfriamientoEnFrames;
    public int frames = 0;
    public int balasDisparo = 2;
    public int municionTotal;
    public int balasMochila = 200;
    string mensajeEnfriamiento;

    public Canvas canvas;
    public RawImage imgBalas1;
    public RawImage imgBalas2;
    public RawImage imgBalas3;
    public RawImage flecha1;
    public RawImage flecha2;
    public RawImage flecha3;

    public GameObject bala;
    public GameObject pivote;

    public Text texto;
    public float deltaTime;

    // Use this for initialization
    void Start()
    {
    cargadores.Add(0);
    cargadores.Add(0);
    cargadores.Add(0);
imgBalas1.rectTransform.localScale = new Vector3(0, 1, 1);
imgBalas2.rectTransform.localScale = new Vector3(0, 1, 1);
imgBalas3.rectTransform.localScale = new Vector3(0, 1, 1);
   
    }

    // Update is called once per frame
    void Update()
    {
        //Gestiona el enfriamiento
        calcularFPS();

        //Calcular enfriamiento (Mas o menos 1 seg)
        if (frames > enfriamientoEnFrames)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                disparar();
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                cambiarCargador();
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            recargar();
        }

       mostrarTexto();
        gestionImagen();
    }

    void gestionImagen()
    {

        float escalaImagen;
        if (cargadores[indiceCargadorPistola] != 0)
            escalaImagen = (float)cargadores[indiceCargadorPistola] / municionMaxima;
        else
            escalaImagen = 0;

        imgBalas1.color=new Color(255,255,255);
        imgBalas2.color=new Color(255,255,255);
        imgBalas3.color=new Color(255,255,255);
        flecha1.rectTransform.localScale = new Vector3(0, 1, 1);
        flecha2.rectTransform.localScale = new Vector3(0, 1, 1);
        flecha3.rectTransform.localScale = new Vector3(0, 1, 1);


        if(indiceCargadorPistola==0){
        imgBalas1.rectTransform.localScale = new Vector3(escalaImagen, 1, 1);
        flecha1.rectTransform.localScale = new Vector3(1, 1, 1);
        imgBalas1.color=new Color(255,0,0);
        }else if(indiceCargadorPistola==1){
        imgBalas2.rectTransform.localScale = new Vector3(escalaImagen, 1, 1);
        imgBalas2.color=new Color(255,0,0);
        flecha2.rectTransform.localScale = new Vector3(1, 1, 1);
        }else if(indiceCargadorPistola==2){
        imgBalas3.rectTransform.localScale = new Vector3(escalaImagen, 1, 1);
        imgBalas3.color=new Color(255,0,0);
        flecha3.rectTransform.localScale = new Vector3(1, 1, 1);
        }
    }

    void mostrarTexto()
    {
        mensajeEnfriamiento = "En enfriamiento\n";
        //Calcular enfriamiento 
        if (frames > enfriamientoEnFrames)
        {
            mensajeEnfriamiento = "PistolaLista\n";
        }

        municionTotal = balasMochila;
        for (int i = 0; i < cargadores.Count; i++)
        {
            municionTotal += cargadores[i];
        }
        string mensaje = "Cargador Seleccionado: " + indiceCargadorSelect + "\n" +
        "Cargador En pistola: " + indiceCargadorPistola + "\n" + mensajeEnfriamiento +
      "Municion: " + cargadores[indiceCargadorPistola] + "/" + municionMaxima + " (" + balasMochila + ")";

        texto.text = mensaje;
    }

void OnCollisionEnter (Collision col)
    {
        //Debug.Log(col.gameObject.name);
         if(col.gameObject.name == "Terrain"){
            Destroy(gameObject);
        }
    }

    void disparar()
    {
        if (cargadores[indiceCargadorPistola] >= balasDisparo)
        {
            
            
            GameObject newBala=new GameObject();
            
            newBala.transform.rotation=transform.rotation;
            //newBala.transform.Rotate(Vector3.left*90f);
            
            newBala = (GameObject) Instantiate(bala, pivote.transform.position, newBala.transform.rotation);
            
            cargadores[indiceCargadorPistola] -= balasDisparo;
            frames = 0;
        }
    }

    void recargar()
    {
        int balasActuales = cargadores[indiceCargadorPistola];
        int balasArecargar = municionMaxima - balasActuales;
        int balasMochilaRestantes = balasMochila - balasArecargar;

        if (balasMochilaRestantes > 0)
        {
            cargadores[indiceCargadorPistola] += balasArecargar;
            balasMochila -= balasArecargar;
        }
        else
        {
            if (balasArecargar + balasMochilaRestantes > 0)
            {
                balasArecargar += balasMochilaRestantes;
                cargadores[indiceCargadorPistola] += balasArecargar;
                balasMochila -= balasArecargar;
            }

        }

    }

    //Cambia el cargador (tarda 60 frames mas o menos en cambiar)
    void cambiarCargador()
    {
        indiceCargadorPistola += 1;
        if (indiceCargadorPistola >= cargadores.Count)
        {
            indiceCargadorPistola = 0;
        }
    }
    void calcularFPS()
    {
        if (frames <= enfriamientoEnFrames)
        {
            frames++;

        }
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;

        enfriamientoEnFrames = Mathf.Ceil(fps);
    }


}
