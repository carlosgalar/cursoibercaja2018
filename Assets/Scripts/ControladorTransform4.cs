﻿// Script: ControladorTransform4.cs
// Descripción: Controlador basico de transform teniendo en cuenta el deltaTime y utilizando las variables
//  estaticas de la clase Vector3 para indicar las direcciones. Las teclas se obtienen por eje y se utilizan
//  variables auxiliares Vector3 para guardar el resultado
// Autor: Ivan Garcia Subero
// Fecha: 25.03.14
// Licencia: Dominio público
// Dependencias: Ninguna
// Por hacer: Nada

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CnControls;

public class ControladorTransform4 : MonoBehaviour {

	public float giroX = 50f;
	public float giroY = 55f;
	public float velocidadZ = 100f;
	public float velocidadCamara = 100f;
	public float acelerar = 1;
	public float numBolas=0;

public Camera camara;
	
	public GameObject bomba; 
	public GameObject helice; 
	public GameObject hijo; 
	public GameObject fantasma; 
	public GameObject posBombas; 
	public Rigidbody rg;

	void Start(){
		 Cursor.visible = false;
	}


	// Update is called once per frame
	void Update ( ) {

		//Vector3 avance = Input.GetAxis ("Vertical") * hijo.transform.forward * velocidadZ *acelerar* Time.deltaTime;
		//transform.Translate ( avance,Space.World );

		Vector3 avance = Input.GetAxis ("Vertical") * transform.forward * velocidadZ *acelerar;
		//transform.Translate ( avance );
		rg.velocity = avance;

		Vector3 rotacion = Input.GetAxis ( "Mouse X" ) * Vector3.up * giroX * Time.deltaTime;
		//Vector3 rotacion = CnInputManager.GetAxis ( "Horizontal" ) * Vector3.up * giroX * Time.deltaTime;
		transform.Rotate ( rotacion );

		Vector3 rotacion2 = Input.GetAxis ( "Mouse Y" ) * Vector3.left * giroY * Time.deltaTime;
		//Vector3 rotacion2 = CnInputManager.GetAxis ( "Vertical" ) * Vector3.left * giroY * Time.deltaTime;
		transform.Rotate ( rotacion2);

		Vector3 rotacionpivote = Input.GetAxis ( "Loop" ) * Vector3.forward * velocidadZ * Time.deltaTime;
		hijo.transform.Rotate ( rotacionpivote*3);
		
		Vector3 rotacion3 = Input.GetAxis ( "Loop" ) * Vector3.forward * velocidadZ * Time.deltaTime;
		transform.Rotate ( -rotacion3);
		
		Vector3 rotacionCamara = Input.GetAxis ( "Mouse ScrollWheel" ) * Vector3.forward * velocidadCamara * Time.deltaTime;
		camara.transform.Translate ( rotacionCamara);


		if ( Input.GetKey ( KeyCode.Space ) ) {
			 Instantiate(bomba, posBombas.transform.position, transform.rotation);
	 numBolas++;
		}

	hijo.transform.rotation=	Quaternion.Lerp(hijo.transform.rotation,fantasma.transform.rotation,0.05f);
	transform.rotation=	Quaternion.Lerp(hijo.transform.rotation,fantasma.transform.rotation,0.05f);
	//transform.rotation=	Quaternion.Lerp(transform.rotation,fantasma.transform.rotation,0.05f);

		if(Input.anyKey){
		helice.transform.Rotate ( Vector3.forward*1000f);
		}
		helice.transform.Rotate ( Vector3.forward*20f);

	}

}

